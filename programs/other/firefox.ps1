choco install Firefox --params "/l:en-GB /NoDesktopShortcut /NoAutoUpdate" -y
& cmd.exe --% /c SetUserFTA http FirefoxHTML
& cmd.exe --% /c SetUserFTA https FirefoxHTML
& cmd.exe --% /c SetUserFTA .htm FirefoxHTML
& cmd.exe --% /c SetUserFTA .html FirefoxHTML

# config
# Download moonlight userchrome theme
$localFile = "$($env:temp)\moonlight-userchrome-$(New-Guid).zip"
Invoke-WebRequest "https://github.com/eduardhojbota/moonlight-userChrome/archive/refs/heads/master.zip" -OutFile $localFile

# ensure profile directory exists
Start-Process firefox
Stop-Process -Name firefox -ErrorAction SilentlyContinue

# locate prefs file
$prefsFile = (Get-ChildItem -Path "$($env:APPDATA)\Mozilla\Firefox\Profiles\**\prefs.js" -Recurse | Select -First 1)

# enable use of userchrome
if (-not (Select-String -Path $prefsfile -Pattern "toolkit.legacyUserProfileCustomizations.stylesheets")) {
    Add-Content -Path $prefsfile -Value 'user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);'
}

# locate and create chrome directory
$targetDir = "$($prefsFile.Directory.FullName)\"
If (-not (Test-Path $targetDir)) {
    New-Item -ItemType Directory $targetDir
}

# extract moonlight-userchrome into chrome directory
& 7z x "$($localFile)" -o"$($targetDir)" -y
if (Test-Path "$($targetDir)\chrome") { Remove-Item "$($targetDir)\chrome" -Force -Recurse; }
Rename-Item "$($targetDir)\moonlight-userChrome-master" "$($targetDir)\chrome" -Force

# locate (new) userChrome file
$userChromeFile = (Get-ChildItem -Path "$($env:APPDATA)\Mozilla\Firefox\Profiles\**\userChrome.css" -Recurse | Select -First 1)

# enable use of userchrome
if (-not (Select-String -Path $userChromeFile -Pattern "_titlebar-controls-enable-windows")) {
    Add-Content -Path $userChromeFile -Value '@import "custom/_titlebar-controls-enable-windows.css";'
}

# hiding native floating tabs
if (-not (Select-String -Path $userChromeFile -Pattern "#TabsToolbar {display: none;}")) {
    Add-Content -Path $userChromeFile -Value '#TabsToolbar {display: none;}'
}

# hiding sidebery sidebar header
if (-not (Select-String -Path $userChromeFile -Pattern "#sidebar-header {display: none;}")) {
    Add-Content -Path $userChromeFile -Value '#sidebar-header {display: none;}'
}

# extensions
Start-Process firefox https://addons.mozilla.org/en-US/firefox/addon/bitwarden-password-manager/
Start-Process firefox https://addons.mozilla.org/en-US/firefox/addon/sidebery/
Start-Process firefox https://addons.mozilla.org/en-GB/firefox/addon/ublock-origin/