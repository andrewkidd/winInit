# about etc

#Requires -RunAsAdministrator
Set-StrictMode -Version Latest
Set-ExecutionPolicy Bypass -Scope Process -Force
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072
$ScriptDir = Split-Path $script:MyInvocation.MyCommand.Path
$ErrorActionPreference = "Stop"
cls

# import modules before copying to profile
Get-ChildItem -Directory -Path ".\profile\Modules" | ForEach-Object {
    Remove-Module -Name $_.FullName -Force -ErrorAction SilentlyContinue
    Import-Module -Name $_.FullName -Force;
}

Write-ASCIIMessage -message "Profile"
Write-TitleMessage -message "Copying profile"
Copy-Item -Path ".\profile" -Destination "$(Split-Path $profile)" -Force

Write-ASCIIMessage -message "Programs"
Get-ChildItem -Recurse -Path "$($ScriptDir)\programs\" -Filter "*.ps1" | ForEach-Object {
    Write-TitleMessage -message "Installing program: $($_.BaseName)"
    & $_.FullName
}

Write-ASCIIMessage -message "Tweaks"
Get-ChildItem -Recurse -Path "$($ScriptDir)\tweaks\" -Filter "*.ps1" | ForEach-Object {
    Write-TitleMessage -message "Executing tweak: $($_.BaseName)"
    & $_.FullName
}

Write-Success -message 'WinInit Complete'
exit