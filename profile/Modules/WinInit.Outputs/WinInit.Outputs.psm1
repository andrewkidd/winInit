Set-StrictMode -Version Latest

function Write-TitleMessage($message) {
    Write-Host "$("=" * $host.UI.RawUI.BufferSize.Width)`r`n$("` " * (($host.UI.RawUI.BufferSize.Width/2) - ($message.length / 2)))$($message)`r`n$("=" * $host.UI.RawUI.BufferSize.Width)" -ForegroundColor White
}

function Write-SuccessMessage($message) {
    Write-Host $message -ForegroundColor Green
}

function Write-ASCIIMessage($message) {

	$bigText = [ordered]@{}
	$bigText['-'] = @("    ", "    ", "    ", "    ", "    ", "    ")
	$bigText[' '] = @("    ", "    ", "    ", "    ", "    ", "    ")
	$bigText['.'] = @("    ", "    ", "    ", "    ", "    ", "    ")
	$bigText['a'] = @("           ", "     /\    ", "    /  \   ", "   / /\ \  ", "  / ____ \ ", " /_/    \_\")
	$bigText['b'] = @("  ____  ", " |  _ \ ", " | |_) |", " |  _ < ", " | |_) |", " |____/ ")
	$bigText['c'] = @("   _____ ", "  / ____|", " | |     ", " | |     ", " | |____ ", "  \_____|")
	$bigText['d'] = @("  _____  ", " |  __ \ ", " | |  | |", " | |  | |", " | |__| |", " |_____/ ")
	$bigText['e'] = @("  ______ ", " |  ____|", " | |__   ", " |  __|  ", " | |____ ", " |______|")
	$bigText['f'] = @("  ______ ", " |  ____|", " | |__   ", " |  __|  ", " | |     ", " |_|     ")
	$bigText['g'] = @("   _____ ", "  / ____|", " | |  __ ", " | | |_ |", " | |__| |", "  \_____|")
	$bigText['h'] = @("  _    _ ", " | |  | |", " | |__| |", " |  __  |", " | |  | |", " |_|  |_|")
	$bigText['i'] = @("  _____ ", " |_   _|", "   | |  ", "   | |  ", "  _| |_ ", " |_____|")
	$bigText['j'] = @("       _ ", "      | |", "      | |", "  _   | |", " | |__| |", "  \____/ ")
	$bigText['k'] = @("  _  __", " | |/ /", " | ' / ", " |  <  ", " | . \ ", " |_|\_\")
	$bigText['l'] = @("  _      ", " | |     ", " | |     ", " | |     ", " | |____ ", " |______|")
	$bigText['m'] = @("  __  __ ", " |  \/  |", " | \  / |", " | |\/| |", " | |  | |", " |_|  |_|")
	$bigText['n'] = @("  _   _ ", " | \ | |", " |  \| |", " | .   |", " | |\  |", " |_| \_|")
	$bigText['o'] = @("   ____  ", "  / __ \ ", " | |  | |", " | |  | |", " | |__| |", "  \____/ ")
	$bigText['p'] = @("  _____  ", " |  __ \ ", " | |__) |", " |  ___/ ", " | |     ", " |_|     ")
	$bigText['q'] = @("   ____  ", "  / __ \ ", " | |  | |", " | |  | |", " | |__| |", "  \___\_\")
	$bigText['r'] = @("  _____  ", " |  __ \ ", " | |__) |", " |  _  / ", " | | \ \ ", " |_|  \_\")
	$bigText['s'] = @("   _____ ", "  / ____|", " | (___  ", "  \___ \ ", "  ____) |", " |_____/ ")
	$bigText['t'] = @("  _______ ", " |__   __|", "    | |   ", "    | |   ", "    | |   ", "    |_|   ")
	$bigText['u'] = @("  _    _ ", " | |  | |", " | |  | |", " | |  | |", " | |__| |", "  \____/ ")
	$bigText['v'] = @(" __      __", " \ \    / /", "  \ \  / / ", "   \ \/ /  ", "    \  /   ", "     \/    ")
	$bigText['w'] = @(" __          __", " \ \        / /", "  \ \  /\  / / ", "   \ \/  \/ /  ", "    \  /\  /   ", "     \/  \/    ")
	$bigText['x'] = @(" __   __", " \ \ / /", "  \ V / ", "   > <  ", "  / . \ ", " /_/ \_\")
	$bigText['y'] = @(" __     __", " \ \   / /", "  \ \_/ / ", "   \   /  ", "    | |   ", "    |_|   ")
	$bigText['z'] = @("  ______", " |___  /", "    / / ", "   / /  ", "  / /__ ", " /_____|")
	$maxLines = ($bigText.keys | ForEach-Object { $bigText[$_].length} | Measure-Object -Max).Maximum

	$return = ""

	for ($i = 0; $i -lt $maxLines; $i++){
		$message.ToChararray() | ForEach-Object {
			$char = $_
			try{
				$return += $bigText["$($_)"][$i]
			} catch {
				write-error "No char for '$($char)'"
			}
		}
		$return += "`r`n"
	}

	return $return
}
