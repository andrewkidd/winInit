@{

    # Script module or binary module file associated with this manifest.
    RootModule = 'WinInit.Outputs.psm1'

    # Version number of this module.
    ModuleVersion = '0.0.1'

    # Cmdlets to export from this module.
    CmdletsToExport = @(
        'Write-TitleMessage',
        'Write-SuccessMessage',
        'Write-ASCIIMessage'
    )

}