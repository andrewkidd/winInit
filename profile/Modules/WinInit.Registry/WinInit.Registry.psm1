Set-StrictMode -Version Latest

function Set-RegKey {
	[CmdletBinding()]
    param(
        [string]$key,
        [string]$name,
        [string]$type,
        [string]$value
    )
	
    If (-Not (Test-Path $key)) {
        Write-Host "Creating key: $key"
        New-Item -Path $key -ItemType Key | Out-Null
    }
	
    $currentValue = ((Get-ItemProperty $key -Name $name) | Select-Object $name).$name
    if ($currentValue -ne $value) {
        Write-Host "Setting key: $key\$name = $value"
        Set-ItemProperty -Path $key -Name $name -Type $type -Value $value
    }
}