Set-StrictMode -Version Latest

Function Get-KeyVaultSecretAsPlaintext($secretName, $vaultName) {
	$encPass = (Get-AzKeyVaultSecret -VaultName $vaultName -name $secretName).SecretValue
	Write-Output ([System.Net.NetworkCredential]::new("", $encPass).Password)
}

Function Remove-AzDNSRecords([string[]]$recordNames = @(), [string]$dnsZone, [string]$recordType = 'CNAME') {
	$recordNames | ForEach-Object {
		Write-Output "Removing: $($_).$($dnsZone)"
		Get-AzDnsRecordSet -ZoneName $dnsZone -ResourceGroupName $dnsZone -Name $_ -RecordType $recordType | Remove-AzDnsRecordSet
	}
}

Function Restart-AzResourceGroup($resourceGroupName, $includeSlots = 0) {
	$webapps = Get-AzWebApp -ResourceGroupName $resourceGroupName
	$webapps | ForEach-Object {
	if ($includeSlots -ne 0) {
			$slots = (Get-AzWebAppSlot -Webapp $_)
			$slots | ForEach-Object {
				Write-Output "Restarting $($_.Name)"
				(($_ |Restart-AzWebAppSlot ) | Out-Null)
		}
	}
	   Write-Output "Restarting $($_.Name)"
	   (($_|Restart-AzWebApp) | Out-Null)
	}
}

Function Restart-AzAppServicePlan($appServicePlanName, $resourceGroupName) {
	$appServicePlan = Get-AzAppServicePlan -Name $appServicePlanName -ResourceGroupName $resourceGroupName
	$webapps = Get-AzWebApp -AppServicePlan $appServicePlan
	$webapps | ForEach-Object {
    if ($includeSlots -ne 0) {
	        $slots = (Get-AzWebAppSlot -Webapp $_)
	        $slots | ForEach-Object {
	            Write-Output "Restarting $($_.Name)"
	            (($_ |Restart-AzWebAppSlot ) | Out-Null)
 	       }
	       Write-Output "Restarting $($_.Name)"
	       (($_|Restart-AzWebApp) | Out-Null)
	    }
    }
}


Function Stop-AzAppServicePlan($appServicePlanName, $resourceGroupName, $includeSlots = 0) {
	$appServicePlan = Get-AzAppServicePlan -Name $appServicePlanName -ResourceGroupName $resourceGroupName
	$webapps = Get-AzWebApp -AppServicePlan $appServicePlan
	$webapps | ForEach-Object {
		if ($includeSlots -ne 0) {
 			$slots = (Get-AzWebAppSlot -Webapp $_)
	   		$slots | ForEach-Object {
	       		 	Write-Output "Stopping $($_.Name)"
	        		(($_ |Stop-AzWebAppSlot ) | Out-Null)
 	   		}
		}
	   Write-Output "Stopping $($_.Name)"
	   (($_|Stop-AzWebApp) | Out-Null)
	}
}

Function Start-AzAppServicePlan($appServicePlanName, $resourceGroupName, $includeSlots = 0) {
	$appServicePlan = Get-AzAppServicePlan -Name $appServicePlanName -ResourceGroupName $resourceGroupName
	$webapps = Get-AzWebApp -AppServicePlan $appServicePlan
	$webapps | ForEach-Object {
		if ($includeSlots -ne 0) {
 			$slots = (Get-AzWebAppSlot -Webapp $_)
	   		$slots | ForEach-Object {
	       		 	Write-Output "Starting $($_.Name)"
	        		(($_ |Start-AzWebAppSlot ) | Out-Null)
 	   		}
		}
	   Write-Output "Starting $($_.Name)"
	   (($_|Start-AzWebApp) | Out-Null)
	}
}

Function Get-AppServicePlanBreakdown($resourceGroupName) {
	$x = Get-AzAppServicePlan
	$y=@{}
	$x | ForEach-Object {
		if ($y[$_.Sku.Name] -eq $null){
			$y[$_.Sku.Name] = @()
		}
		$y[$_.Sku.Name] += $_.Name
	}
	$y | Format-table
	return $y
}