@{

    # Script module or binary module file associated with this manifest.
    RootModule = 'WinInit.Azure.psm1'

    # Version number of this module.
    ModuleVersion = '0.0.1'

    # Cmdlets to export from this module.
    CmdletsToExport = @(
        'Get-KeyVaultSecretAsPlaintext',
        'Remove-AzDNSRecords',
        'Restart-AzResourceGroup',
        'Restart-AzAppServicePlan',
        'Stop-AzAppServicePlan',
        'Start-AzAppServicePlan',
        'Get-AppServicePlanBreakdown'
    )

}