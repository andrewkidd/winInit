$env:Path += ";$(split-path $profile)\Scripts"

## Change colors of regular text
$Host.UI.RawUI.BackGroundColor = "DarkMagenta"
$Host.UI.RawUI.ForeGroundColor = "DarkYellow" 

## Change colors of special messages (defaults shown)
$Host.PrivateData.DebugBackgroundColor = "Black"
$Host.PrivateData.DebugForegroundColor = "Yellow"
$Host.PrivateData.ErrorBackgroundColor = "Black"
$Host.PrivateData.ErrorForegroundColor = "White"
$Host.PrivateData.ProgressBackgroundColor = "DarkCyan"
$Host.PrivateData.ProgressForegroundColor = "Yellow"
$Host.PrivateData.VerboseBackgroundColor = "Black"
$Host.PrivateData.VerboseForegroundColor = "Yellow"
$Host.PrivateData.WarningBackgroundColor = "Black"
$Host.PrivateData.WarningForegroundColor = "Yellow"

Write-ASCIIMessage -message $env:UserName