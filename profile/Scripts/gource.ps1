param(

    [Parameter(Mandatory=$false)]
    [string]
    $repoPath = ""
)
$ErrorActionPreference = "Stop"

# while repo path is invalid
while (
    ($repoPath -eq "") -or 
    ($repoPath -ne "" -and !(Test-Path -Path (Resolve-Path $repoPath)))
    ) {
    $repoPath = Read-Host 'Which Repo?'
}

$repoName = Split-Path (Resolve-Path $repoPath) -Leaf;
gource --load-config gource.ini --output-framerate 60 --output-ppm-stream "$($repoName).ppm" --path $repoPath
ffmpeg.exe -y -r 60 -f image2pipe -vcodec ppm -i "$($repoName).ppm" -vcodec wmv1 -r 60 -qscale 0 "$($repoName).wmv"
Remove-Item "$($repoName).ppm" -Force -ErrorAction SilentlyContinue