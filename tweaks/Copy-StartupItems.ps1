Get-ChildItem ..\assets\startup | ForEach-Object {
	Write-Output "Copying Startup file '$($_.Name)'"
	Copy-Item $_.FullName "$($env:ProgramData)\Microsoft\Windows\Start Menu\Programs\StartUp\$($_.Name)" -Force
}