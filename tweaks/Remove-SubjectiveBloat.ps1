# this is a mish-mash of various windows settings
# see thread: https://gist.github.com/NickCraver/7ebf9efbfd0c3eab72e9

##################
# Privacy Settings
##################

# Privacy: Let apps use my advertising ID: Disable
Set-RegKey -type DWORD -key "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\AdvertisingInfo" -name "Enabled" -value 0

# Privacy: SmartScreen Filter for Store Apps: Disable
Set-RegKey -type DWORD -key "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\AppHost" -name "EnableWebContentEvaluation" -value 0

# WiFi Sense: HotSpot Sharing: Disable
Set-RegKey -type DWORD -key "HKLM:\Software\Microsoft\PolicyManager\default\WiFi\AllowWiFiHotSpotReporting" -name "value" -value 0

# WiFi Sense: Shared HotSpot Auto-Connect: Disable
Set-RegKey -type DWORD -key "HKLM:\Software\Microsoft\PolicyManager\default\WiFi\AllowAutoConnectToWiFiSenseHotspot" -name "value" -value 0

# Activity Tracking: Disable
Set-RegKey -type DWORD -key "HKLM:\SOFTWARE\Policies\Microsoft\Windows\System" -name "EnableActivityFeed" -value 0
Set-RegKey -type DWORD -key "HKLM:\SOFTWARE\Policies\Microsoft\Windows\System" -name "PublishUserActivities" -value 0
Set-RegKey -type DWORD -key "HKLM:\SOFTWARE\Policies\Microsoft\Windows\System" -name "UploadUserActivities" -value 0

# Start Menu: Disable Bing Search Results
Set-RegKey -type DWORD -key "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Search" -name "BingSearchEnabled" -value 0

# Start Menu: Disable Cortana
Set-RegKey -type DWORD -key "HKLM:\SOFTWARE\Policies\Microsoft\Windows\Windows Search" -name "AllowCortana" -value 0

# Disable Telemetry
Set-RegKey -type DWORD -key "HKLM:\SOFTWARE\Policies\Microsoft\Windows\DataCollection" -name "AllowTelemetry" -value 0
 
# show known file extensions and hidden files
Set-RegKey -type DWORD -key "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "Hidden" -value 1
Set-RegKey -type DWORD -key "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "HideFileExt" -value 0
Set-RegKey -type DWORD -key "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "ShowSuperHidden" -value 1

# stop the related services
Get-Service DiagTrack,Dmwappushservice | Stop-Service | Set-Service -StartupType Disabled

# remove appx packages
@(
    "king.com.CandyCrushSaga",
    "Microsoft.BingWeather",
    "Microsoft.BingNews",
    "Microsoft.BingSports",
    "Microsoft.BingFinance",
    "Microsoft.XboxApp",
    "Microsoft.WindowsPhone",
    "Microsoft.MicrosoftSolitaireCollection",
    "Microsoft.People",
    "Microsoft.ZuneMusic",
    "Microsoft.ZuneVideo",
    "Microsoft.Office.OneNote",
    "Microsoft.Windows.Photos",
    "Microsoft.WindowsSoundRecorder",
    "microsoft.windowscommunicationsapps",
    "Microsoft.SkypeApp"
 ) | ForEach-Object {
    $pkg =  Get-AppxPackage $_
    if ($pkg) {
        Write-Host "Removing Package: $($_)"
        $pkg | Remove-AppxPackage
    }
 }

# Uninstall OneDrive cos i use nextcloud
if (Test-Path $env:SystemRoot\SysWOW64\OneDriveSetup.exe) { & $env:SystemRoot\SysWOW64\OneDriveSetup.exe /uninstall } else { & $env:SystemRoot\System32\OneDriveSetup.exe /uninstall }