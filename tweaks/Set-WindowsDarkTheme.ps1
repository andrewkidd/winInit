$ErrorActionPreference = "Stop"

# Force create key if doesn't exist
New-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize -Name AppsUseLightTheme -Value 0 -ErrorAction SilentlyContinue

# check current value
$currentValue = ((Get-ItemProperty HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize -Name AppsUseLightTheme) | Select-Object AppsUseLightTheme).AppsUseLightTheme
if ($currentValue -ne 0) {
    # update and reset
    Set-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize -Name AppsUseLightTheme -Value 0
    
    # restart explorer
    Stop-Process -Name explorer
    Start-Process explorer
}

# set wallpaper
$wallpaperFilename = "..\assets\wallpaper.png"
Set-Wallpaper -Image $wallpaperFilename -Style Fit

# Windows Accent color
Set-RegKey -type DWORD -key "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Accent" -name 'AccentColorMenu' -value '0xff2a1a19'
Set-RegKey -type BINARY -key "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Accent" -name 'AccentPalette' -value @(0x4F, 0x53, 0x86, 0xFF, 0x42, 0x45, 0x6F, 0xFF, 0x39, 0x38, 0x5F, 0xFF, 0x2F, 0x31, 0x4F, 0xFF, 0x26, 0x27, 0x3F, 0xFF, 0x1C, 0x1D, 0x2F, 0xFF, 0x0F, 0x0F, 0x19, 0xFF, 0x88, 0x17, 0x98, 0x00)
Set-RegKey -type DWORD -key "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Accent" -name 'MotionAccentId_v1.00' -value '0x000000db'
Set-RegKey -type DWORD -key "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Accent" -name 'StartColorMenu' -value '0xff3f2726'
Set-RegKey -type DWORD -key "HKCU:\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize" -name 'EnableTransparency' -value '0'

Stop-Process -ProcessName explorer -Force -ErrorAction SilentlyContinue